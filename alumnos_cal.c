#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>

FILE *alumnos, *contadores;

// FRANCISCO JAVER VARGAS LOPEZ 20310075

struct datos{
	float cal;
	char nombre[20], apellido[20];
};

void capturar();
void buscar();
int maximo(int cont1);
int maximo1();


int main(){
	int menu;
	do{
		system("cls");
		system("color F0");
	printf("===============================================================================");
   	printf("\n||                                                                          ||\n");
	printf("||                    Captura de Calificaciones de                          ||\n");   	
	printf("||                             Alumnos                                      ||\n"); 
	printf("||                                                                          ||"); 
   	printf("\n===============================================================================\n");
		printf("(1) Capturar alumnos\n(2) Buscar alumno\n(3) Salir\n\n     >>");
		scanf("%d", &menu);
		switch (menu){
			case 1: capturar(); break;
			case 2: buscar(); break;
		}
	
	}while(menu!=3);
	
	return 0;
}

void capturar(){

	struct datos x;
	int cont=0;
	
		alumnos = fopen("alumnos.dat", "ab");
		system("cls");
		if(alumnos==NULL){
			printf("\n\nERROR AL CREAR EL ARCHIVO\n\n");
		}
		else{
		cont++;
		printf("Nombre del alumno: ");
		fflush (stdin);
		gets(x.nombre);
		printf("Apellido: ");
		fflush (stdin);
		gets(x.apellido);
		printf("Calificacion: ");
		scanf("%f", &x.cal);
		maximo(cont);
		fwrite(&x, sizeof(struct datos)*1, 1, alumnos);

		fclose(alumnos);
		
		}
		system("pause");

}

void buscar(){
	struct datos d;
	int elegir, a, x=0;
	float cal;
	char nom[50], ape[50];
	 
	alumnos = fopen("alumnos.dat", "rb");
	
	if(alumnos==NULL){
		printf("\n\nARCHIVO INEXISTENTE\n\n");
	
	}
	else{
		x=maximo1();
		printf("Buscar alumno por\n\n(1) Nombre y Calificacion\n(2) Apellido y Calificacion\n\n     >>");
		scanf("%d", &elegir);
		if(elegir==1){
			printf("Ingrese el nombre y calificacion del alumno\n\n     >>");
			fflush(stdin);
			gets(nom);
			printf("\n\n     >>");
			scanf("%d", &cal);
			
			
			system("cls");
			system("color 0F");
			printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n                            Buscando alumno");
			for(int f=0;f<5;f++){
				printf(" . ");
				Sleep(500);
			}
			system("cls"); 
			
			for(int i=0; i<x; i++){
				fread(&d, sizeof(struct datos)*1, 1, alumnos);			
				if(strcmp(d.nombre, nom) == 0 && cal == d.cal){
						system("color A0");
	printf("===============================================================================");
   	printf("\n||                                                                          ||\n");
	printf("||                           Alumno encontrado                              ||\n");   	
	printf("||                                                                          ||"); 
   	printf("\n===============================================================================\n");
						printf("\Nombre: %d", d.nombre);
						printf("\Apellido: %s", d.apellido);
						printf("\Calificacion: %f", d.cal);
						a=1;			
			    }	
			}
			if(a!=1){
				system("color C0");
	printf("===============================================================================");
   	printf("\n||                                                                          ||\n");
	printf("||                           Alumno no encontrado                           ||\n");   	
	printf("||                                                                          ||"); 
   	printf("\n===============================================================================\n");
				printf("\n\nNo encontrado o no se encuentra capturado\n\n");
			}
					
			}  
		    else{
			 if(elegir==2){
			 	
				printf("Ingrese apellido y calificacion del alumno: ");
				fflush(stdin);
				gets(ape);
				printf("\n\n     >>");
				scanf("%d", &cal);
				
			system("cls");
			system("color 0F");
			printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n                            Buscando libro");
			for(int f=0;f<5;f++){
				printf(" . ");
				Sleep(500);
			}
			system("cls");
			
			    for(int j=0; j<x; j++){
				
					fread(&d, sizeof(struct datos)*1, 1, libros);	
					 if(strcmp(d.apellido, ape) == 0 && cal == d.cal){
					 	system("color A0");
	printf("===============================================================================");
   	printf("\n||                                                                          ||\n");
	printf("||                           Alumno encontrado                              ||\n");   	
	printf("||                                                                          ||"); 
   	printf("\n===============================================================================\n");
						printf("\Nombre: %d", d.nombre);
						printf("\Apellido: %s", d.apellido);
						printf("\Calificacion: %f", d.cal);
						a=1;
					}
				}
				if(a!=1){
					system("color C0");
	printf("===============================================================================");
   	printf("\n||                                                                          ||\n");
	printf("||                        Alumno no encontrado                              ||\n");   	
	printf("||                                                                          ||"); 
   	printf("\n===============================================================================\n");
				printf("\n\nNo encontrado o no se encuentra capturado\n\n");
			}
				 }
			}
		 
		fclose(alumnos);
	}
system("pause");
}

int maximo(int cont1){
	int total;
	contadores = fopen("contar.txt", "a");
	if(contadores== NULL){
		return 0;
	}else{
		fprintf(contadores, "%d ", cont1);
		fclose(contadores);
	}
	return 0;
}

int maximo1(){
	int total=0, num=0;
	contadores=fopen("contar.txt", "r");
	
	if(contadores==NULL){
	}else{
		while(!feof(contadores)){
		
		   fscanf(contadores, "%d\n", &num);
		   if(num==1){
		   	total++;
		   }
	    }
	     return total;
	    fclose(contadores);
	}
	  return total;
}


